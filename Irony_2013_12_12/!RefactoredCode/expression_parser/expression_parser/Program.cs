﻿using Nielsen.Core;
using System;
using System.Web;

namespace expression_parser
{
    class Program
    {
        static void Main(string[] args) {

            String URL = "http://ukr.net/TESTERS/Default6.aspx/?par1=4&par2=5";
            //String URL = "http://localhost:1302/TESTERS/Default6.aspx/?par1=4&par2=5";
            //String URL = "?wf=TestLink&p2=lskdj";
            
            var res = ConvertUrl(URL);
            string expr = "(a + b) * 3.14 / (2.0 * x - y*z*2)";
            
            Console.Write("Input expression: ");
            expr = Console.ReadLine();

            var parser = new KnockoutExpression();
            var result = parser.Generate(expr);
            Console.WriteLine(string.Format("Result: {0}", result));
            Console.ReadLine();
        }

        static string ConvertUrl(string url) {
            int pos = url.IndexOf("?");
            if (pos == -1)
            {
                return url;
            }

            string parameters = url.Substring(pos + 1);
            var res = string.Format("{0}?{1}", url.Substring(0, pos), HttpUtility.HtmlEncode(parameters));
            return res;
        }
    }
}
