﻿using Irony.Parsing;
using Irony.Interpreter.Evaluator;

namespace Nielsen.Core
{
    public class KnockoutExpression : IExpressionParser
    {
        public string Generate(string expression)
        {
            var grammar = new ExpressionEvaluatorGrammar();
            var language = new LanguageData(grammar);
            var parser = new Parser(language);

            var tree = parser.Parse(expression);
            if (tree.Status == ParseTreeStatus.Error)
            {
                return tree.Status.ToString();
            }

            var res = ParseNode(tree.Root);
            return res;
        }

        string ParseNode(ParseTreeNode node)
        {
            var currentNode = node.ChildNodes.Count == 0 ? node : node.ChildNodes[0];
            if (node.ChildNodes.Count == 0 || node.ChildNodes.Count == 1)
            {
                switch (currentNode.Term.Name)
                {
                    case "identifier":
                        return string.Format("self.{0}()", currentNode.Token.Value);
                    case "number":
                        return currentNode.Token.ValueString;
                    case "BinExpr":
                        var leftExpr = ParseNode(currentNode.ChildNodes[0]);
                        var rightExpr = ParseNode(currentNode.ChildNodes[2]);
                        var operation = currentNode.ChildNodes[1].Term.Name;
                        return string.Format("({0} {1} {2})", leftExpr, operation, rightExpr);
                    default:
                        break;
                }

            }
            else if (node.ChildNodes.Count == 3)
            {
                var leftExpr = ParseNode(node.ChildNodes[0]);
                var rightExpr = ParseNode(node.ChildNodes[2]);
                var operation = node.ChildNodes[1].Term.Name;
                return string.Format("({0} {1} {2})", leftExpr, operation, rightExpr);
            }

            return string.Empty;
        }

    }
}
