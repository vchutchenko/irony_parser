﻿using Irony.Interpreter.Evaluator;
using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nielsen.Core
{
	public class ExpressionGrammar : ExpressionEvaluatorGrammar
	{
		public ExpressionGrammar()
		{
			// 1. Terminals
			NumberLiteral number = new NumberLiteral("number");
			Terminal v = new IdentifierTerminal("variable");

			// 2. Non-terminals
			NonTerminal Expr = new NonTerminal("Expr");
			NonTerminal BinOp = new NonTerminal("BinOp");
			NonTerminal UnOp = new NonTerminal("UnOp");
			NonTerminal ExprLine = new NonTerminal("ExprLine");

			// 3. BNF rules
			Expr.Rule = number | v | Expr + BinOp + Expr | UnOp + Expr | "(" + Expr + ")";

			BinOp.Rule = ToTerm("+") | "-" | "*" | "/" | "**";
			UnOp.Rule = "-";            //Irony provides default conversion here 
			ExprLine.Rule = Expr + Eof; //Eof is a predefined EOF terminal
			this.Root = ExprLine;             //Set grammar top element

			// 4. Set operators precedence and associativity
			RegisterOperators(1, "+", "-");
			RegisterOperators(2, "*", "/");
			RegisterOperators(3, Associativity.Right, "**");

			// 5. Register parenthesis as punctuation symbols
			//    so they will not appear in the syntax tree
			RegisterBracePair("(", ")");

		}
	}
}
