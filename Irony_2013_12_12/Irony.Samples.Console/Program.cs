﻿using System;
using Nielsen.Core;

namespace Irony.Samples.ConsoleCalculator
{
    class Program {

    static void Main(string[] args) {
            string expr = "(a + b) * 3.14 / (2.0 * x - y*z*2)";

            Console.Write("Input expression: ");
            expr = Console.ReadLine();

            var parser = new KnockoutExpression();
            var result = parser.Generate(expr);
            Console.WriteLine(string.Format("Result: {0}", result));
            Console.ReadLine();
        }


    }


}
