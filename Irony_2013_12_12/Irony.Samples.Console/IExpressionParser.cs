﻿namespace Nielsen.Core
{
    interface IExpressionParser
    {
        string Generate(string expression);
    }
}
